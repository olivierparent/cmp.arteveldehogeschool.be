#!/usr/bin/env bash

mysql --user=homestead --password=secret --execute="GRANT ALL PRIVILEGES ON cmp_arteveldehogeschool_be.* TO 'cmp_db_user' IDENTIFIED BY 'cmp_db_password'"
mysql --user=cmp_db_user --password=cmp_db_password --execute="CREATE DATABASE IF NOT EXISTS cmp_arteveldehogeschool_be CHARACTER SET utf8 COLLATE utf8_general_ci"
