<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp_arteveldehogeschool_be');

/** MySQL database username */
define('DB_USER', 'cmp_db_user');

/** MySQL database password */
define('DB_PASSWORD', 'cmp_db_password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wIN]X.Z$&~}rQOb%meN)HRfcv$CDu@^^ J-xcp=Sp,^aW-|Ap1-HIm+/*,YA`[R^');
define('SECURE_AUTH_KEY',  'QHv-;]/CV~/.xn0fPp1-NU=wwM+TMRW2#_5nzkFCYwKmvB+B24fQyc@h-Zm|5je+');
define('LOGGED_IN_KEY',    '[-wzSG+UNB^|U?_MQscL5NReG(%XhSj)lOS|:C6~e/-Kmp06@}Pv6,+Qotw|3fhN');
define('NONCE_KEY',        'l&3qBG)nv<`4CGb]W_Xz%H=o/rM~C@~bv-.=A`.l#;h*)D(`w}(>B3j8@2V%RamE');
define('AUTH_SALT',        'cWQ}.I ~iQ|R]fY&0r:@ @IzM]83jU|}y61)c7d|Mc8YB8=HZ 3-LSfz/@a)Sl$g');
define('SECURE_AUTH_SALT', 'I/VRB:]_+|2OoI`6CJC3}y:E^,g!JruM<5J@W]-<4$`9:M-w]v-}I=h=$y0B5<Wc');
define('LOGGED_IN_SALT',   '{7(v+!t,uy6v=&DL;poD^]cDnn9DwI38m|f4Z!.LEELL/:K;6,:U_u,<.ZM ^r2W');
define('NONCE_SALT',       'nVox7->+xTw40Fd-uX=3&5U_YG,S.4=SaL#K|`@pTNnmshPC7Zgn.r-W([! .{1m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
